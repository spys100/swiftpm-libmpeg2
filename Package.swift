// swift-tools-version:5.5
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

let package = Package(
    name: "libmpeg2",
    products: [
        .library(name: "LibMpeg2", targets: ["LibMpeg2"]),
    ],
    targets: [
        .target(
            name: "CLibMpeg2",
            path: "./Sources/libmpeg2"
        ),
        .target(
            name: "LibMpeg2",
            dependencies: [
              .target (name: "CLibMpeg2"/*, condition: .when(platforms: [.iOS])*/)
            ],
            path: "./Sources/mpeg2swift"
        )
    ]
)
