//
//  File.swift
//  
//
//  Created by Carsten Kroll on 24.03.22.
//

import Foundation
import CoreMedia
import CLibMpeg2


public typealias Mpeg2DecoderOutputCallback = (CVImageBuffer?, CMTime, CMTime) -> Void

public class Mpeg2Decoder{
  
  var handler:Mpeg2DecoderOutputCallback
  var context:UnsafeMutableRawPointer?
  var callback:decoder_cb_t = { refcon,width,height,chroma_width,chroma_height,y,u,v,fnum,dur,pts,pixAspect  in
    guard let refcon = refcon
          else {
            print("Mpeg2DecoderOutputCallback error")
            return }
    var pb:CVPixelBuffer?
    
    let fourCC = kCVPixelFormatType_420YpCbCr8BiPlanarVideoRange
       
    let flags = [
      kCVPixelBufferIOSurfacePropertiesKey: [:] as CFDictionary,
    ] as CFDictionary
    
    let status = CVPixelBufferCreate(kCFAllocatorDefault, Int(width), Int(height), fourCC, flags, &pb)
          
    guard let pb = pb else { return }
    let width = Int(width)
    let height = Int(width)
    let chroma_width = Int(chroma_width)
    let chroma_height = Int(chroma_height)
    
//    if #available(tvOS 13.0, *) {
//      debugPrint(pb.attachments)
//    }
    let att = [ kCVImageBufferPixelAspectRatioKey: [
      kCVImageBufferPixelAspectRatioHorizontalSpacingKey: pixAspect.nom,
      kCVImageBufferPixelAspectRatioVerticalSpacingKey: pixAspect.den
    ]] as CFDictionary
    CVBufferSetAttachments(pb, att, CVAttachmentMode.shouldPropagate)
//    if #available(tvOS 13.0, *) {
//      debugPrint(pb.attachments)
//    }
    CVPixelBufferLockBaseAddress(pb, []);
    for (i,src,srcWidth,srcHeight) in [(0,y,width,height)]{
      let bytesPerRow = CVPixelBufferGetBytesPerRowOfPlane(pb, i)
      let dst = CVPixelBufferGetBaseAddressOfPlane(pb, i)
      if  dst != nil {
        for row in 0..<srcHeight {
          memcpy(dst! + row * bytesPerRow, src! + row * srcWidth, srcWidth)
        }
      }
    }
    for (srcWidth,srcHeight) in [(chroma_width,chroma_height)]{
      let bytesPerRow = CVPixelBufferGetBytesPerRowOfPlane(pb, 1)
      let h = CVPixelBufferGetHeightOfPlane(pb, 1)
      let dst = CVPixelBufferGetBaseAddressOfPlane(pb, 1)
  
      if  dst != nil {
        let dp = UnsafeMutablePointer<UInt8>(OpaquePointer(dst))
        let dstP = UnsafeMutableBufferPointer<UInt8>(start: dp, count: h * bytesPerRow)
        for row in 0..<srcHeight {
          for col in 0..<srcWidth {
            let dOffset = row * bytesPerRow + (col * 2)
            let sOffset = row * srcWidth + col
            dstP[dOffset] = u![sOffset]
            dstP[dOffset + 1] = v![sOffset]
          }
        }
      }
    }

    CVPixelBufferUnlockBaseAddress(pb, []);

    let decoder:Mpeg2Decoder = Unmanaged<Mpeg2Decoder>.fromOpaque(refcon).takeUnretainedValue()
    //debugPrint("pts_out: \(pts * 1000000.0)")
    decoder.handler(pb,CMTime(seconds: pts,preferredTimescale: 1000000),CMTime(seconds: dur ,preferredTimescale: 1000000))
  }
  
  public init(handler: @escaping Mpeg2DecoderOutputCallback) {
    self.handler = handler
    self.context = mpg_init(Unmanaged.passUnretained(self).toOpaque(),callback)
  }
  deinit{
    debugPrint("Mpeg2Decoder deinit")
    mpg_close(context)
  }
  
  /// decode sampleBuffer
  public func decode(_ sampleBuffer: CMSampleBuffer) {
    guard let blockBuffer = CMSampleBufferGetDataBuffer(sampleBuffer) else { return }
    //let dur = CMSampleBufferGetDuration(sampleBuffer).seconds
    let pts = CMSampleBufferGetPresentationTimeStamp(sampleBuffer)
    //debugPrint("pts_in: \(pts.seconds * 1000000.0)")
    if #available(macOS 10.15, iOS 13.0, tvOS 13.0, watchOS 6.0, *) {
      do {
        try blockBuffer.withUnsafeMutableBytes() { buf in
          let op = OpaquePointer(buf.baseAddress)
          let p = UnsafePointer<UInt8>(op)
          guard let context=context else {return}
          mpg_decode(context,pts.seconds,p,buf.count)
         }
      }
      catch {
        debugPrint("Error")
      }
    }else{
      //CMBlockBufferGetDataPointer(blockBuffer, atOffset: 0, lengthAtOffsetOut: nil, totalLengthOut: nil, dataPointerOut: p)
    }
  }
}
