

#ifndef public_h
#define public_h

struct fraction_s {
  int nom;
  int den;
};

typedef void (*decoder_cb_t)(void *refcon, int width, int height, int chroma_width, int chroma_height,const unsigned char *y,const unsigned char *u,const unsigned char *v, long fnum, double dur, double pts, struct fraction_s pixelAspect);

void* mpg_init(void *ref, decoder_cb_t cb);
void mpg_close(void *context);

void mpg_decode (void *context,double pts, const unsigned char *buffer, long  size);

#endif /* public_h */
