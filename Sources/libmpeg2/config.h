/* autodetect accelerations */
#define ACCEL_DETECT

/* maximum supported data alignment */
#define ATTRIBUTE_ALIGNED_MAX 64

/* Define to `__inline__' or `__inline' if that's what the C compiler
   calls it, or to nothing if 'inline' is not supported under any name.  */
#ifndef __cplusplus
#define inline __attribute__ ((__always_inline__))
#endif

#if __ARM_ARCH == 4
#define ARCH_ARM
#endif

#ifdef __x86_64
#define ARCH_X86_64
#endif
