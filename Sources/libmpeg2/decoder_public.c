/*
 * Copyright (C) 2003      Regis Duchesne <hpreg@zoy.org>
 * Copyright (C) 2000-2003 Michel Lespinasse <walken@zoy.org>
 * Copyright (C) 1999-2000 Aaron Holtzman <aholtzma@ess.engr.uvic.ca>
 * Copyright (C) 2006      Sam Hocevar <sam@zoy.org>
 * Copyright (C) 2022      Carsten Kroll <c a r_(at) x- i m i d i .com>
 * based on sample5.c
 * mpeg2dec is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * mpeg2dec is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * - Output buffers use the YUV 4:2:0 planar format.
 * - Output buffers must be 16-byte aligned.
 * - Output buffers are allocated and managed by the caller.
 */

#include <stdlib.h>
#include <inttypes.h>

#include "mpeg2.h"
#include "libmpeg2/public.h"

#define ALIGN_16(p) ((void *)(((uintptr_t)(p) + 15) & ~((uintptr_t)15)))

#define MHZ_27 27000000.0

typedef struct {
  mpeg2dec_t *decoder;
  decoder_cb_t decoder_cb;
  void *refcon;
  struct fbuf_s {
    uint8_t * mbuf[3];
    uint8_t * yuv[3];
    int used;
    double pts;
  } fbuf[3];
} context_t;


static struct fbuf_s * get_fbuf (context_t * context)
{
    int i;

    for (i = 0; i < 3; i++)
    if (!context->fbuf[i].used) {
        context->fbuf[i].used = 1;
        return context->fbuf + i;
    }
}

void* mpg_init(void *ref, decoder_cb_t cb)
{
  
  context_t* context= malloc(sizeof(context_t));
  context->refcon = ref;
  context->decoder = mpeg2_init ();
  context->decoder_cb = cb;
  return context;
}

void mpg_close(void *context){
  mpeg2_close(((context_t*)context)->decoder);
  free(context);
}

void mpg_decode (void *context,double pts, const unsigned char *buffer, long  size)
{
  mpeg2dec_t *decoder = ((context_t*)context)->decoder;
  struct fbuf_s *fbuf = ((context_t*)context)->fbuf;
  const mpeg2_info_t * info;
  const mpeg2_sequence_t * sequence;
  mpeg2_state_t state;
  long consumed = 0;
  int framenum = 0;
  int i, j;
  struct fbuf_s * current_fbuf;
 
  if (!decoder) return;

  info = mpeg2_info (decoder);
  
  do {
    state = mpeg2_parse (decoder);
    sequence = info->sequence;
    switch (state) {
      case STATE_BUFFER:
        if (consumed < size){
          mpeg2_buffer (decoder, buffer, buffer + (size - consumed));
          consumed += size;
          buffer += consumed;
        }else{
          consumed = 0;
        }
        break;
      case STATE_SEQUENCE:
        mpeg2_custom_fbuf (decoder, 1);
        for (i = 0; i < 3; i++) {
          fbuf[i].mbuf[0] = (uint8_t *) malloc (sequence->width *
                   sequence->height + 15);
          fbuf[i].mbuf[1] = (uint8_t *) malloc (sequence->chroma_width *
                   sequence->chroma_height + 15);
          fbuf[i].mbuf[2] = (uint8_t *) malloc (sequence->chroma_width *
                   sequence->chroma_height + 15);
          if (!fbuf[i].mbuf[0] || !fbuf[i].mbuf[1] || !fbuf[i].mbuf[2]) {
            return;//TODO: return error code
          }
          for (j = 0; j < 3; j++)
              fbuf[i].yuv[j] = ALIGN_16 (fbuf[i].mbuf[j]);
          fbuf[i].used = 0;
        }
        for (i = 0; i < 2; i++) {
          current_fbuf = get_fbuf (context);
          current_fbuf->pts = pts;
          mpeg2_set_buf (decoder, current_fbuf->yuv, current_fbuf);
        }
        break;
      case STATE_PICTURE:
          current_fbuf = get_fbuf (context);
          current_fbuf->pts = pts;
          mpeg2_set_buf (decoder, current_fbuf->yuv, current_fbuf);
          break;
      case STATE_SLICE:
      case STATE_END:
      case STATE_INVALID_END:
        if (info->display_fbuf){
          struct fraction_s pixAspect = {1,1};
          pixAspect.nom = info->sequence->pixel_width;
          pixAspect.den = info->sequence->pixel_height;
            if (((context_t*)context)->decoder_cb) {
              uint8_t *y=info->display_fbuf->buf[0];
              uint8_t *u=info->display_fbuf->buf[1];
              uint8_t *v=info->display_fbuf->buf[2];
              double pts=((struct fbuf_s*)(info->display_fbuf->id))->pts;
              (*((context_t*)context)->decoder_cb)(((context_t*)context)->refcon,sequence->width, sequence->height, sequence->chroma_width, sequence->chroma_height,
                                                   y,u,v,framenum++,
                                                   ((double)sequence->frame_period)/MHZ_27,pts,
                                                   pixAspect);
            }
        }
        if (info->discard_fbuf)
              ((struct fbuf_s *)info->discard_fbuf->id)->used = 0;
        if (state != STATE_SLICE)
        for (i = 0; i < 3; i++)
            for (j = 0; j < 3; j++)
              free (fbuf[i].mbuf[j]);
        break;
      default:
        break;
    }
  }while(consumed);

}

